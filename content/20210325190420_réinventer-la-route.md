# On réinvente la roue

Je ne sais pas exactement pourquoi mais j'ai eu envie de me faire un blog. Comme quand pendant mes études et que j'y connaissais rien. C'est plutôt rigolo 👴

Bref.

Un [repo gitlab](https://framagit.org/stadja/blog), un petit [.htaccess](https://framagit.org/stadja/blog/-/blob/master/.htaccess) futé, un peu de PHP, du [Markdown](https://michelf.ca/projects/php-markdown/extra/), un poil de JS, du CSS et bim on y est. 

Il me reste à mettre les commentaires...

💖