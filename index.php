<?php
date_default_timezone_set('Europe/Paris');
// --- La setlocale() fonctionnne pour strftime mais pas pour DateTime->format()
setlocale(LC_TIME, 'fr_FR.utf8','fra');// OK

require __DIR__ . '/vendor/autoload.php';
$root = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';
$root = '//'.str_replace('//', '/', $root);
// use Michelf\Markdown;
use Michelf\MarkdownExtra as Markdown;
function minimizeCSSsimple($css)
{
  $css = preg_replace('/\/\*((?!\*\/).)*\*\//', '', $css); // negative look ahead
  $css = preg_replace('/\s{2,}/', ' ', $css);
  $css = preg_replace('/\s*([:;{}])\s*/', '$1', $css);
  $css = preg_replace('/;}/', '}', $css);
  return $css;
}
function extractContentFromPage($pageName)
{
  $text = file_get_contents('./content/' . $pageName);
  $html = Markdown::defaultTransform($text);
  return $html;
}
function getPages()
{
  $pages = [];
  $files = scandir(getcwd() . '/content', SCANDIR_SORT_DESCENDING);
  if (isset($_GET['page'])) {
  	$files = [$_GET['page']];
  }
  foreach ($files as $file) {
    if ($file == '.' || $file == '..') {
      continue;
    }
    $page = [];
    $page['file'] = $file;
    $page['html'] = extractContentFromPage($file);
    $page['dateTime'] = \DateTime::createFromFormat('YmdHis', explode('_', $file)[0]);
    $slug = str_replace('_', '/', $file);
    $slug = str_replace('.md', '', $slug);
    $page['url'] = 'blog/'.$slug;
    $pages[] = $page;
  }

  return $pages;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<title>Mon souffle sent le silicium</title>
		<meta name="description" content="un blog, du vent">
		<style type="text/css" media="screen"><?php echo minimizeCSSsimple(file_get_contents('assets/css/style.css')); ?></style>
		<base href="<?php echo $root; ?>">
	</head>
	<body>
		<header class="font--title">
			<p class="header--biggest" style="font-size: 11.089em;">Mon souffle sent le silicium</p>
			<p class="header--big" style="font-size: 6.853em;">Mon souffle sent le silicium</p>
			<p style="font-size: 4.236em;">Mon souffle sent le silicium</p>
			<span style="font-size: 2.618em;"><mark><a href="">Mon souffle sent le silicium</a></mark></span>
			<p style="font-size: 1.618em;">Mon souffle sent le silicium</p>
			<p>Mon souffle sent le silicium</p>
		</header>
		<p class="header-sub">Parfois je suis tout contre mon écran</p>
		<section class="content">
			<?php foreach (getPages() as $page): ?>
			<article>
				<?php echo $page['html']; ?>
				<aside class="aside">
					<a href="<?php echo $page['url'] ?>"><?php echo strftime('%A, le %x à %T', $page['dateTime']->format('U')); ?></a>
				</aside>
			</article>
			<?php endforeach;?>
		</section>
		<script>
			let header = document.getElementsByTagName('header')[0];
			let titles = header.querySelectorAll('p')
			for (let i = 0; i < titles.length; i++) {
				let title = titles[i];
				let text = title.innerText;
				let innerHTML = "";
				for (let j = 0; j < text.length; j++) {
					innerHTML += (text[j] == " ") ? ' ' : '<span class="title-letter" style="color: hsl('+Math.floor(360*Math.random())+',var(--color-saturation),var(--color-lightness))">'+text[j]+'</span>';
				}
				titles[i].innerHTML = innerHTML;
			}
		</script>
	</body>
</html>
