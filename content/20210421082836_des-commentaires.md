# Des commentaires ?

Non, un channel IRC.

Finalement je suis parti sur un channel IRC avec un bot.
C'est une vieille technologie qui fonctionne depuis longtemps et qui ne consomme pas beaucoup de mémoire (et pas besoin de gérer une base de données pour rien).

Du coup: 

- j'ai ouvert un channel IRC sur [hybridirc](https://hybridirc.com/) (mais ça aurait pu être [freenode](https://freenode.net/) ou autre)
- j'y ai [ajouté un bot](https://github.com/jhuckaby/simplebot) 
- Ce bot je l'ai mis en opérator
- J'ai changé le mode du channel pour lui dire d'afficher les 50 dernières lignes des 10 dernières années à la nouvelle connection - `/mode +H 50:10y`
- et c'est tout :)

Que ça devienne un chat ou un endroit de question, ou un channel vide... et bein voilà, au moins tout est là et j'ai plus besoin de m'en occuper !

 [💬](https://thelounge.hybridirc.com?join=mssls)